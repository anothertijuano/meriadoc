library ieee;
use ieee.std_logic_1164.all;

--------------
--000     ADD
--001     SUB
--100     XOR
--101     OR
--110     AND
--------------
entity ALU is 
     port(  
          X         :in std_logic_vector(31 downto 0);
          Y         :in std_logic_vector(31 downto 0);
          F         :in std_logic_vector(2 downto 0);
          Z         :out std_logic_vector(31 downto 0);
          enable    :in std_logic 
         ); 
end ALU;

architecture behavioral of ALU is
     component ADDER_32 is 
          port( 
               cin  :in std_logic; 
               X    :in std_logic_vector(31 downto 0);
               Y    :in std_logic_vector(31 downto 0);
               cout :out std_logic;
               Z    :out std_logic_vector(31 downto 0)
          ); 
     end component;

     component BUFF_32 is
          port(
               input     :in  std_logic_vector(31 downto 0);
               output    :out std_logic_vector(31 downto 0);
               enable    :in  std_logic
          );
     end component;

     signal cout :std_logic; 
     signal cin  :std_logic; 
     signal tmpY :std_logic_vector(31 downto 0);
     signal ADDo :std_logic_vector(31 downto 0):=x"00000000";
     signal tmpZ :std_logic_vector(31 downto 0);
begin
     ADDER: ADDER_32 port map(cin,X,tmpY,cout,ADDo);
     OPERATION: process(F,X,Y,ADDo)
     begin
          --ADDo<=ADDo;
          case F is
               when "000" =>  cin <= '0';
                              tmpY <= Y;
                              tmpZ <= ADDo;
               when "001" =>  cin <= '1';
                              tmpY <= not(Y);
                              tmpZ <= ADDo;
               when "100" =>  tmpZ <= X xor Y;
               when "101" =>  tmpZ <= X or Y;
               when "110" =>  tmpZ <= X and Y;
               when others => tmpZ <= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
          end case;
     end process;
     OUTPUT: BUFF_32 port map(tmpZ,Z,enable);
end behavioral; 
