library ieee;
use ieee.std_logic_1164.all;

entity BUFF_32 is 
     port(  
          input     :in  std_logic_vector(31 downto 0);
          output    :out std_logic_vector(31 downto 0);
          enable    :in  std_logic
     ); 
end BUFF_32;

architecture behavioral of BUFF_32 is
     component TRI_BUFF is
          port(
               input     : in  std_logic;
               output    : out std_logic;
               enable    : in  std_logic
          );
     end component;
begin 
     O_31: TRI_BUFF port map(input(31), output(31), enable);
     O_30: TRI_BUFF port map(input(30), output(30), enable);
     O_29: TRI_BUFF port map(input(29), output(29), enable);
     O_28: TRI_BUFF port map(input(28), output(28), enable);
     O_27: TRI_BUFF port map(input(27), output(27), enable);
     O_26: TRI_BUFF port map(input(26), output(26), enable);
     O_25: TRI_BUFF port map(input(25), output(25), enable);
     O_24: TRI_BUFF port map(input(24), output(24), enable);
     O_23: TRI_BUFF port map(input(23), output(23), enable);
     O_22: TRI_BUFF port map(input(22), output(22), enable);
     O_21: TRI_BUFF port map(input(21), output(21), enable);
     O_20: TRI_BUFF port map(input(20), output(20), enable);
     O_19: TRI_BUFF port map(input(19), output(19), enable);
     O_18: TRI_BUFF port map(input(18), output(18), enable);
     O_17: TRI_BUFF port map(input(17), output(17), enable);
     O_16: TRI_BUFF port map(input(16), output(16), enable);
     O_15: TRI_BUFF port map(input(15), output(15), enable);
     O_14: TRI_BUFF port map(input(14), output(14), enable);
     O_13: TRI_BUFF port map(input(13), output(13), enable);
     O_12: TRI_BUFF port map(input(12), output(12), enable);
     O_11: TRI_BUFF port map(input(11), output(11), enable);
     O_10: TRI_BUFF port map(input(10), output(10), enable);
     O_09: TRI_BUFF port map(input(9),  output(9),  enable);
     O_08: TRI_BUFF port map(input(8),  output(8),  enable);
     O_07: TRI_BUFF port map(input(7),  output(7),  enable);
     O_06: TRI_BUFF port map(input(6),  output(6),  enable);
     O_05: TRI_BUFF port map(input(5),  output(5),  enable);
     O_04: TRI_BUFF port map(input(4),  output(4),  enable);
     O_03: TRI_BUFF port map(input(3),  output(3),  enable);
     O_02: TRI_BUFF port map(input(2),  output(2),  enable);
     O_01: TRI_BUFF port map(input(1),  output(1),  enable);
     O_00: TRI_BUFF port map(input(0),  output(0),  enable);
end behavioral; 
