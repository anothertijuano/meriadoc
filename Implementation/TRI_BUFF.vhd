library ieee;
use ieee.std_logic_1164.all;

entity TRI_BUFF is
     port(
          input     : in  std_logic;
          output    : out std_logic;
          enable    : in  std_logic
         );
end TRI_BUFF;

architecture behavioral of TRI_BUFF is
begin
     output <= input when (enable='0') else 'Z';
end behavioral;
