library ieee;
use ieee.std_logic_1164.all;

entity REG_32 is 
     port(  
          DEST : in  std_logic_vector(31 downto 0);
          S1   : out std_logic_vector(31 downto 0);
          S2   : out std_logic_vector(31 downto 0);
          rdN  : in  std_logic;
          rs1N : in  std_logic;
          rs2N : in  std_logic
     ); 
end REG_32;

architecture behavioral of REG_32 is
     
     component TRI_BUFF is
          port(
               input     : in  std_logic;
               output    : out std_logic;
               enable    : in  std_logic
          );
     end component;
     
     component D_FF is
          port(
               D    : in std_logic;
               CLK  : in std_logic;
               Q    : out std_logic
          );
     end component;

     --Output of FlipFlops and input of Buffers
     signal Q  : std_logic_vector(31 downto 0):=x"00000000";
begin 
     --DEST bus to FlipFlops
     Q_31 : D_FF port map(DEST(31),rdn,Q(31));
     Q_30 : D_FF port map(DEST(30),rdn,Q(30));
     Q_29 : D_FF port map(DEST(29),rdn,Q(29));
     Q_28 : D_FF port map(DEST(28),rdn,Q(28));
     Q_27 : D_FF port map(DEST(27),rdn,Q(27));
     Q_26 : D_FF port map(DEST(26),rdn,Q(26));
     Q_25 : D_FF port map(DEST(25),rdn,Q(25));
     Q_24 : D_FF port map(DEST(24),rdn,Q(24));
     Q_23 : D_FF port map(DEST(23),rdn,Q(23));
     Q_22 : D_FF port map(DEST(22),rdn,Q(22));
     Q_21 : D_FF port map(DEST(21),rdn,Q(21));
     Q_20 : D_FF port map(DEST(20),rdn,Q(20));
     Q_19 : D_FF port map(DEST(19),rdn,Q(19));
     Q_18 : D_FF port map(DEST(18),rdn,Q(18));
     Q_17 : D_FF port map(DEST(17),rdn,Q(17));
     Q_16 : D_FF port map(DEST(16),rdn,Q(16));
     Q_15 : D_FF port map(DEST(15),rdn,Q(15));
     Q_14 : D_FF port map(DEST(14),rdn,Q(14));
     Q_13 : D_FF port map(DEST(13),rdn,Q(13));
     Q_12 : D_FF port map(DEST(12),rdn,Q(12));
     Q_11 : D_FF port map(DEST(11),rdn,Q(11));
     Q_10 : D_FF port map(DEST(10),rdn,Q(10));
     Q_09 : D_FF port map(DEST(9),rdn,Q(9));
     Q_08 : D_FF port map(DEST(8),rdn,Q(8));
     Q_07 : D_FF port map(DEST(7),rdn,Q(7));
     Q_06 : D_FF port map(DEST(6),rdn,Q(6));
     Q_05 : D_FF port map(DEST(5),rdn,Q(5));
     Q_04 : D_FF port map(DEST(4),rdn,Q(4));
     Q_03 : D_FF port map(DEST(3),rdn,Q(3));
     Q_02 : D_FF port map(DEST(2),rdn,Q(2));
     Q_01 : D_FF port map(DEST(1),rdn,Q(1));
     Q_00 : D_FF port map(DEST(0),rdn,Q(0));
     
     --Buffers to S1 bus
     S1_31: TRI_BUFF port map(Q(31),S1(31),rs1N);
     S1_30: TRI_BUFF port map(Q(30),S1(30),rs1N);
     S1_29: TRI_BUFF port map(Q(29),S1(29),rs1N);
     S1_28: TRI_BUFF port map(Q(28),S1(28),rs1N);
     S1_27: TRI_BUFF port map(Q(27),S1(27),rs1N);
     S1_26: TRI_BUFF port map(Q(26),S1(26),rs1N);
     S1_25: TRI_BUFF port map(Q(25),S1(25),rs1N);
     S1_24: TRI_BUFF port map(Q(24),S1(24),rs1N);
     S1_23: TRI_BUFF port map(Q(23),S1(23),rs1N);
     S1_22: TRI_BUFF port map(Q(22),S1(22),rs1N);
     S1_21: TRI_BUFF port map(Q(21),S1(21),rs1N);
     S1_20: TRI_BUFF port map(Q(20),S1(20),rs1N);
     S1_19: TRI_BUFF port map(Q(19),S1(19),rs1N);
     S1_18: TRI_BUFF port map(Q(18),S1(18),rs1N);
     S1_17: TRI_BUFF port map(Q(17),S1(17),rs1N);
     S1_16: TRI_BUFF port map(Q(16),S1(16),rs1N);
     S1_15: TRI_BUFF port map(Q(15),S1(15),rs1N);
     S1_14: TRI_BUFF port map(Q(14),S1(14),rs1N);
     S1_13: TRI_BUFF port map(Q(13),S1(13),rs1N);
     S1_12: TRI_BUFF port map(Q(12),S1(12),rs1N);
     S1_11: TRI_BUFF port map(Q(11),S1(11),rs1N);
     S1_10: TRI_BUFF port map(Q(10),S1(10),rs1N);
     S1_09: TRI_BUFF port map(Q(9),S1(9),rs1N);
     S1_08: TRI_BUFF port map(Q(8),S1(8),rs1N);
     S1_07: TRI_BUFF port map(Q(7),S1(7),rs1N);
     S1_06: TRI_BUFF port map(Q(6),S1(6),rs1N);
     S1_05: TRI_BUFF port map(Q(5),S1(5),rs1N);
     S1_04: TRI_BUFF port map(Q(4),S1(4),rs1N);
     S1_03: TRI_BUFF port map(Q(3),S1(3),rs1N);
     S1_02: TRI_BUFF port map(Q(2),S1(2),rs1N);
     S1_01: TRI_BUFF port map(Q(1),S1(1),rs1N);
     S1_00: TRI_BUFF port map(Q(0),S1(0),rs1N);
     
     --Buffers to S1 bus
     S2_31: TRI_BUFF port map(Q(31),S2(31),rs2N);
     S2_30: TRI_BUFF port map(Q(30),S2(30),rs2N);
     S2_29: TRI_BUFF port map(Q(29),S2(29),rs2N);
     S2_28: TRI_BUFF port map(Q(28),S2(28),rs2N);
     S2_27: TRI_BUFF port map(Q(27),S2(27),rs2N);
     S2_26: TRI_BUFF port map(Q(26),S2(26),rs2N);
     S2_25: TRI_BUFF port map(Q(25),S2(25),rs2N);
     S2_24: TRI_BUFF port map(Q(24),S2(24),rs2N);
     S2_23: TRI_BUFF port map(Q(23),S2(23),rs2N);
     S2_22: TRI_BUFF port map(Q(22),S2(22),rs2N);
     S2_21: TRI_BUFF port map(Q(21),S2(21),rs2N);
     S2_20: TRI_BUFF port map(Q(20),S2(20),rs2N);
     S2_19: TRI_BUFF port map(Q(19),S2(19),rs2N);
     S2_18: TRI_BUFF port map(Q(18),S2(18),rs2N);
     S2_17: TRI_BUFF port map(Q(17),S2(17),rs2N);
     S2_16: TRI_BUFF port map(Q(16),S2(16),rs2N);
     S2_15: TRI_BUFF port map(Q(15),S2(15),rs2N);
     S2_14: TRI_BUFF port map(Q(14),S2(14),rs2N);
     S2_13: TRI_BUFF port map(Q(13),S2(13),rs2N);
     S2_12: TRI_BUFF port map(Q(12),S2(12),rs2N);
     S2_11: TRI_BUFF port map(Q(11),S2(11),rs2N);
     S2_10: TRI_BUFF port map(Q(10),S2(10),rs2N);
     S2_09: TRI_BUFF port map(Q(9),S2(9),rs2N);
     S2_08: TRI_BUFF port map(Q(8),S2(8),rs2N);
     S2_07: TRI_BUFF port map(Q(7),S2(7),rs2N);
     S2_06: TRI_BUFF port map(Q(6),S2(6),rs2N);
     S2_05: TRI_BUFF port map(Q(5),S2(5),rs2N);
     S2_04: TRI_BUFF port map(Q(4),S2(4),rs2N);
     S2_03: TRI_BUFF port map(Q(3),S2(3),rs2N);
     S2_02: TRI_BUFF port map(Q(2),S2(2),rs2N);
     S2_01: TRI_BUFF port map(Q(1),S2(1),rs2N);
     S2_00: TRI_BUFF port map(Q(0),S2(0),rs2N);

end behavioral; 
