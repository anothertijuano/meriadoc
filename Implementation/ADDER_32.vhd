library ieee;
use ieee.std_logic_1164.all;

entity ADDER_32 is 
     port( 
          cin  :in std_logic; 
          X    :in std_logic_vector(31 downto 0);
          Y    :in std_logic_vector(31 downto 0);
          cout :out std_logic;
          Z    :out std_logic_vector(31 downto 0)
     ); 
end ADDER_32;

architecture behavioral of ADDER_32 is

     component FULL_ADDER is
          port(
               A    :in std_logic;
               B    :in std_logic;
               Cin  :in std_logic;
               S    :out std_logic;
               Cout :out std_logic
          );
     end component;
     signal c: std_logic_vector(30 downto 0);
begin 
     A31: FULL_ADDER port map( X(31), Y(31), C(30), Z(31), cout  );
     A30: FULL_ADDER port map( X(30), Y(30), C(29), Z(30), c(30) );
     A29: FULL_ADDER port map( X(29), Y(29), C(28), Z(29), c(29) );
     A28: FULL_ADDER port map( X(28), Y(28), C(27), Z(28), c(28) );
     A27: FULL_ADDER port map( X(27), Y(27), C(26), Z(27), c(27) );
     A26: FULL_ADDER port map( X(26), Y(26), C(25), Z(26), c(26) );
     A25: FULL_ADDER port map( X(25), Y(25), C(24), Z(25), c(25) );
     A24: FULL_ADDER port map( X(24), Y(24), C(23), Z(24), c(24) );
     A23: FULL_ADDER port map( X(23), Y(23), C(22), Z(23), c(23) );
     A22: FULL_ADDER port map( X(22), Y(22), C(21), Z(22), c(22) );
     A21: FULL_ADDER port map( X(21), Y(21), C(20), Z(21), c(21) );
     A20: FULL_ADDER port map( X(20), Y(20), C(19), Z(20), c(20) );
     A19: FULL_ADDER port map( X(19), Y(19), C(18), Z(19), c(19) );
     A18: FULL_ADDER port map( X(18), Y(18), C(17), Z(18), c(18) );
     A17: FULL_ADDER port map( X(17), Y(17), C(16), Z(17), c(17) );
     A16: FULL_ADDER port map( X(16), Y(16), C(15), Z(16), c(16) );
     A15: FULL_ADDER port map( X(15), Y(15), C(14), Z(15), c(15) );
     A14: FULL_ADDER port map( X(14), Y(14), C(13), Z(14), c(14) );
     A13: FULL_ADDER port map( X(13), Y(13), C(12), Z(13), c(13) );
     A12: FULL_ADDER port map( X(12), Y(12), C(11), Z(12), c(12) );
     A11: FULL_ADDER port map( X(11), Y(11), C(10), Z(11), c(11) );
     A10: FULL_ADDER port map( X(10), Y(10), C(9),  Z(10), c(10) );
     A09: FULL_ADDER port map( X(9),  Y(9),  C(8),  Z(9),  c(9)  );
     A08: FULL_ADDER port map( X(8),  Y(8),  C(7),  Z(8),  c(8)  );
     A07: FULL_ADDER port map( X(7),  Y(7),  C(6),  Z(7),  c(7)  );
     A06: FULL_ADDER port map( X(6),  Y(6),  C(5),  Z(6),  c(6)  );
     A05: FULL_ADDER port map( X(5),  Y(5),  C(4),  Z(5),  c(5)  );
     A04: FULL_ADDER port map( X(4),  Y(4),  C(3),  Z(4),  c(4)  );
     A03: FULL_ADDER port map( X(3),  Y(3),  C(2),  Z(3),  c(3)  );
     A02: FULL_ADDER port map( X(2),  Y(2),  C(1),  Z(2),  c(2)  );
     A01: FULL_ADDER port map( X(1),  Y(1),  C(0),  Z(1),  c(1)  );
     A00: FULL_ADDER port map( X(0),  Y(0),  cin,   Z(0),  c(0)  );
end behavioral; 
