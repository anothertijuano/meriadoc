--4 to 16 DEMUX_H 
library ieee;
use ieee.std_logic_1164.all;

entity DEMUX_H is 
     port( 
          enb  :in  std_logic;
          S    :in  std_logic_vector(3 downto 0);
          O    :out std_logic_vector(15 downto 0)
     ); 
end DEMUX_H;

architecture behavioral of DEMUX_H is
signal tmp: std_logic_vector(15 downto 0):=x"0000";

begin 
     sel: process (S) is
     begin
          case S is
               when x"0" =>   tmp <= x"0001";
               when x"1" =>   tmp <= x"0002";
               when x"2" =>   tmp <= x"0004";
               when x"3" =>   tmp <= x"0008";
               when x"4" =>   tmp <= x"0010";
               when x"5" =>   tmp <= x"0020";
               when x"6" =>   tmp <= x"0040";
               when x"7" =>   tmp <= x"0080";
               when x"8" =>   tmp <= x"0100";
               when x"9" =>   tmp <= x"0200";
               when x"A" =>   tmp <= x"0400";
               when x"B" =>   tmp <= x"0800";
               when x"C" =>   tmp <= x"1000";
               when x"D" =>   tmp <= x"2000";
               when x"E" =>   tmp <= x"4000";
               when others => tmp <= x"8000";
          end case;
     end process;
     
     enable: process (enb) is
     begin
          if(enb='0') then
               O<=tmp;
          else
               O<=x"FFFF";
          end if;
     end process;

end behavioral; 
