library ieee;
use ieee.std_logic_1164.all;

entity DISPLAY is 
     port(  
          in32 :in std_logic_vector(31 downto 0);
          send :in std_logic;
          clk  :in std_logic;
          rw   :out std_logic;
          rs   :out std_logic;
          e    :out std_logic;
          lcdO :out std_logic_vector(7 downto 0)
     ); 
end DISPLAY;

architecture behavioral of DISPLAY is
     component LCD is
          PORT(
               clk        : IN    STD_LOGIC;  --system clock
               reset_n    : IN    STD_LOGIC;  --active low reinitializes lcd
               lcd_enable : IN    STD_LOGIC;  --latches data into lcd controller
               lcd_bus    : IN    STD_LOGIC_VECTOR(9 DOWNTO 0);  --data and control signals
               busy       : OUT   STD_LOGIC := '1';  --lcd controller busy/idle feedback
               rw, rs, e  : OUT   STD_LOGIC;  --read/write, setup/data, and enable for lcd
               lcd_data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0) --data signals for lcd
          );
     end component;

     signal lcd_enable   :std_logic;
     signal lcd_bus      :std_logic_vector(9 downto 0);
     signal lcd_busy     :std_logic;
     signal tmp_bus      :std_logic_vector(79 downto 0);
begin 

     SCREEN: LCD port map(clk,'1',lcd_enable,lcd_bus,lcd_busy,rw,rs,e,lcdO);
  
     char0: process(in32)
     begin
          case in32(31 downto 28) is
               when x"0" => tmp_bus(79 downto 70) <= "1000110001";
               when x"1" => tmp_bus(79 downto 70) <= "1000110001";
               when x"2" => tmp_bus(79 downto 70) <= "1000110010";
               when x"3" => tmp_bus(79 downto 70) <= "1000110011";
               when x"4" => tmp_bus(79 downto 70) <= "1000110100";
               when x"5" => tmp_bus(79 downto 70) <= "1000110101";
               when x"6" => tmp_bus(79 downto 70) <= "1000110110";
               when x"7" => tmp_bus(79 downto 70) <= "1000110111";
               when x"8" => tmp_bus(79 downto 70) <= "1000111000";
               when x"9" => tmp_bus(79 downto 70) <= "1000111001";
               when x"A" => tmp_bus(79 downto 70) <= "1001000001";
               when x"B" => tmp_bus(79 downto 70) <= "1001000010";
               when x"C" => tmp_bus(79 downto 70) <= "1001000011";
               when x"D" => tmp_bus(79 downto 70) <= "1001000100";
               when x"E" => tmp_bus(79 downto 70) <= "1001000101";
               when x"F" => tmp_bus(79 downto 70) <= "1001000110";
               when others => tmp_bus(79 downto 70) <= "0000000000";
          end case;
     end process;
  
     char1: process(in32)
     begin
          case in32(27 downto 24) is
               when x"0" => tmp_bus(69 downto 60) <= "1000110001";
               when x"1" => tmp_bus(69 downto 60) <= "1000110001";
               when x"2" => tmp_bus(69 downto 60) <= "1000110010";
               when x"3" => tmp_bus(69 downto 60) <= "1000110011";
               when x"4" => tmp_bus(69 downto 60) <= "1000110100";
               when x"5" => tmp_bus(69 downto 60) <= "1000110101";
               when x"6" => tmp_bus(69 downto 60) <= "1000110110";
               when x"7" => tmp_bus(69 downto 60) <= "1000110111";
               when x"8" => tmp_bus(69 downto 60) <= "1000111000";
               when x"9" => tmp_bus(69 downto 60) <= "1000111001";
               when x"A" => tmp_bus(69 downto 60) <= "1001000001";
               when x"B" => tmp_bus(69 downto 60) <= "1001000010";
               when x"C" => tmp_bus(69 downto 60) <= "1001000011";
               when x"D" => tmp_bus(69 downto 60) <= "1001000100";
               when x"E" => tmp_bus(69 downto 60) <= "1001000101";
               when x"F" => tmp_bus(69 downto 60) <= "1001000110";
               when others => tmp_bus(69 downto 60) <= "0000000000";
          end case;
     end process;
  
     char2: process(in32)
     begin
          case in32(23 downto 20) is
               when x"0" => tmp_bus(59 downto 50) <= "1000110001";
               when x"1" => tmp_bus(59 downto 50) <= "1000110001";
               when x"2" => tmp_bus(59 downto 50) <= "1000110010";
               when x"3" => tmp_bus(59 downto 50) <= "1000110011";
               when x"4" => tmp_bus(59 downto 50) <= "1000110100";
               when x"5" => tmp_bus(59 downto 50) <= "1000110101";
               when x"6" => tmp_bus(59 downto 50) <= "1000110110";
               when x"7" => tmp_bus(59 downto 50) <= "1000110111";
               when x"8" => tmp_bus(59 downto 50) <= "1000111000";
               when x"9" => tmp_bus(59 downto 50) <= "1000111001";
               when x"A" => tmp_bus(59 downto 50) <= "1001000001";
               when x"B" => tmp_bus(59 downto 50) <= "1001000010";
               when x"C" => tmp_bus(59 downto 50) <= "1001000011";
               when x"D" => tmp_bus(59 downto 50) <= "1001000100";
               when x"E" => tmp_bus(59 downto 50) <= "1001000101";
               when x"F" => tmp_bus(59 downto 50) <= "1001000110";
               when others => tmp_bus(59 downto 50) <= "0000000000";
          end case;
     end process;
  
     char3: process(in32)
     begin
          case in32(19 downto 16) is
               when x"0" => tmp_bus(49 downto 40) <= "1000110001";
               when x"1" => tmp_bus(49 downto 40) <= "1000110001";
               when x"2" => tmp_bus(49 downto 40) <= "1000110010";
               when x"3" => tmp_bus(49 downto 40) <= "1000110011";
               when x"4" => tmp_bus(49 downto 40) <= "1000110100";
               when x"5" => tmp_bus(49 downto 40) <= "1000110101";
               when x"6" => tmp_bus(49 downto 40) <= "1000110110";
               when x"7" => tmp_bus(49 downto 40) <= "1000110111";
               when x"8" => tmp_bus(49 downto 40) <= "1000111000";
               when x"9" => tmp_bus(49 downto 40) <= "1000111001";
               when x"A" => tmp_bus(49 downto 40) <= "1001000001";
               when x"B" => tmp_bus(49 downto 40) <= "1001000010";
               when x"C" => tmp_bus(49 downto 40) <= "1001000011";
               when x"D" => tmp_bus(49 downto 40) <= "1001000100";
               when x"E" => tmp_bus(49 downto 40) <= "1001000101";
               when x"F" => tmp_bus(49 downto 40) <= "1001000110";
               when others => tmp_bus(49 downto 40) <= "0000000000";
          end case;
     end process;
  
     char4: process(in32)
     begin
          case in32(15 downto 12) is
               when x"0" => tmp_bus(39 downto 30) <= "1000110001";
               when x"1" => tmp_bus(39 downto 30) <= "1000110001";
               when x"2" => tmp_bus(39 downto 30) <= "1000110010";
               when x"3" => tmp_bus(39 downto 30) <= "1000110011";
               when x"4" => tmp_bus(39 downto 30) <= "1000110100";
               when x"5" => tmp_bus(39 downto 30) <= "1000110101";
               when x"6" => tmp_bus(39 downto 30) <= "1000110110";
               when x"7" => tmp_bus(39 downto 30) <= "1000110111";
               when x"8" => tmp_bus(39 downto 30) <= "1000111000";
               when x"9" => tmp_bus(39 downto 30) <= "1000111001";
               when x"A" => tmp_bus(39 downto 30) <= "1001000001";
               when x"B" => tmp_bus(39 downto 30) <= "1001000010";
               when x"C" => tmp_bus(39 downto 30) <= "1001000011";
               when x"D" => tmp_bus(39 downto 30) <= "1001000100";
               when x"E" => tmp_bus(39 downto 30) <= "1001000101";
               when x"F" => tmp_bus(39 downto 30) <= "1001000110";
               when others => tmp_bus(39 downto 30) <= "0000000000";
          end case;
     end process;
  
     char5: process(in32)
     begin
          case in32(11 downto 8) is
               when x"0" => tmp_bus(29 downto 20) <= "1000110001";
               when x"1" => tmp_bus(29 downto 20) <= "1000110001";
               when x"2" => tmp_bus(29 downto 20) <= "1000110010";
               when x"3" => tmp_bus(29 downto 20) <= "1000110011";
               when x"4" => tmp_bus(29 downto 20) <= "1000110100";
               when x"5" => tmp_bus(29 downto 20) <= "1000110101";
               when x"6" => tmp_bus(29 downto 20) <= "1000110110";
               when x"7" => tmp_bus(29 downto 20) <= "1000110111";
               when x"8" => tmp_bus(29 downto 20) <= "1000111000";
               when x"9" => tmp_bus(29 downto 20) <= "1000111001";
               when x"A" => tmp_bus(29 downto 20) <= "1001000001";
               when x"B" => tmp_bus(29 downto 20) <= "1001000010";
               when x"C" => tmp_bus(29 downto 20) <= "1001000011";
               when x"D" => tmp_bus(29 downto 20) <= "1001000100";
               when x"E" => tmp_bus(29 downto 20) <= "1001000101";
               when x"F" => tmp_bus(29 downto 20) <= "1001000110";
               when others => tmp_bus(29 downto 20) <= "0000000000";
          end case;
     end process;
  
     char6: process(in32)
     begin
          case in32(7 downto 4) is
               when x"0" => tmp_bus(19 downto 10) <= "1000110001";
               when x"1" => tmp_bus(19 downto 10) <= "1000110001";
               when x"2" => tmp_bus(19 downto 10) <= "1000110010";
               when x"3" => tmp_bus(19 downto 10) <= "1000110011";
               when x"4" => tmp_bus(19 downto 10) <= "1000110100";
               when x"5" => tmp_bus(19 downto 10) <= "1000110101";
               when x"6" => tmp_bus(19 downto 10) <= "1000110110";
               when x"7" => tmp_bus(19 downto 10) <= "1000110111";
               when x"8" => tmp_bus(19 downto 10) <= "1000111000";
               when x"9" => tmp_bus(19 downto 10) <= "1000111001";
               when x"A" => tmp_bus(19 downto 10) <= "1001000001";
               when x"B" => tmp_bus(19 downto 10) <= "1001000010";
               when x"C" => tmp_bus(19 downto 10) <= "1001000011";
               when x"D" => tmp_bus(19 downto 10) <= "1001000100";
               when x"E" => tmp_bus(19 downto 10) <= "1001000101";
               when x"F" => tmp_bus(19 downto 10) <= "1001000110";
               when others => tmp_bus(19 downto 10) <= "0000000000";
          end case;
     end process;
  
     char7: process(in32)
     begin
          case in32(3 downto 0) is
               when x"0" => tmp_bus(9 downto 0) <= "1000110001";
               when x"1" => tmp_bus(9 downto 0) <= "1000110001";
               when x"2" => tmp_bus(9 downto 0) <= "1000110010";
               when x"3" => tmp_bus(9 downto 0) <= "1000110011";
               when x"4" => tmp_bus(9 downto 0) <= "1000110100";
               when x"5" => tmp_bus(9 downto 0) <= "1000110101";
               when x"6" => tmp_bus(9 downto 0) <= "1000110110";
               when x"7" => tmp_bus(9 downto 0) <= "1000110111";
               when x"8" => tmp_bus(9 downto 0) <= "1000111000";
               when x"9" => tmp_bus(9 downto 0) <= "1000111001";
               when x"A" => tmp_bus(9 downto 0) <= "1001000001";
               when x"B" => tmp_bus(9 downto 0) <= "1001000010";
               when x"C" => tmp_bus(9 downto 0) <= "1001000011";
               when x"D" => tmp_bus(9 downto 0) <= "1001000100";
               when x"E" => tmp_bus(9 downto 0) <= "1001000101";
               when x"F" => tmp_bus(9 downto 0) <= "1001000110";
               when others => tmp_bus(9 downto 0) <= "0000000000";
          end case;
     end process;
                    
     send_process: process(send,tmp_bus)
          variable char  :  integer range 0 to 7 := 0;
     begin
          if(send'event and send = '1') then
               if(lcd_busy = '0' AND lcd_enable = '0') then
                    if(char < 8) then
                         char := char + 1;
                    end if;
                    lcd_enable <= '1';
                    case char is
                         when 0 => lcd_bus <= tmp_bus(79 downto 70);
                         when 1 => lcd_bus <= tmp_bus(69 downto 60);
                         when 2 => lcd_bus <= tmp_bus(59 downto 50);
                         when 3 => lcd_bus <= tmp_bus(49 downto 40);
                         when 4 => lcd_bus <= tmp_bus(39 downto 30);
                         when 5 => lcd_bus <= tmp_bus(29 downto 20);
                         when 6 => lcd_bus <= tmp_bus(19 downto 10);
                         when 7 => lcd_bus <= tmp_bus(9 downto 0);
                         when others => lcd_bus <= "0000000000";
                    end case;
               else
                    lcd_enable <= '0';
               end if;
          end if;
     end process;


end behavioral; 
