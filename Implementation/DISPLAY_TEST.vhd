library ieee;
use ieee.std_logic_1164.all;

entity DISPLAY_TB is 
     port(
          clk       : IN  STD_LOGIC;  --system clock
          rw, rs, e : OUT STD_LOGIC;  --read/write, setup/data, and enable for lcd
          lcd_data  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
     ); 

end DISPLAY_TB;

architecture behavioral of DISPLAY_TB is
     component DISPLAY is
          port(  
          in32 :in std_logic_vector(31 downto 0);
          send :in std_logic;
          clk  :in std_logic;
          rw   :out std_logic;
          rs   :out std_logic;
          e    :out std_logic;
          lcdO :out std_logic_vector(7 downto 0)
     ); 
     end component;
     signal in32 :std_logic_vector(31 downto 0);
     signal send :std_logic;
     signal clk  :std_logic;
 
begin 
     uut: DISPLAY port map(in32,send,clk,rw,rs,e,lcd_data);
     
     clk_process: process
     begin
        clk <= '0';
        wait for 0.5 ns;
        clk <= '1';
        wait for 0.5 ns;
     end process;

     stim_process: process
     begin
               in32 <= x"00000000";
               send <= '0';
               wait for 1 ns;
               send <= '1';
               wait for 1 ns;
               in32 <= x"ABABABAB";
               send <= '0';
               wait for 1 ns;
               send <= '1';
               wait for 1 ns;
               in32 <= x"FFFFFFFF";
               send <= '0';
               wait for 1 ns;
               send <= '1';
               wait for 1 ns;
               wait;
     end process;
end behavioral; 
