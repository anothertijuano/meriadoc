# Meriadoc a RISC-V implementation

An agile, brave and incomplete RISC-V one week implementation in VHDL.

The idea is to build a very simple CPU, inspired by the RV32E v1.9, for the Altera IV FPGA. Nothing production ready but simple to understand and explain.

All the work is base upon [The RISCV Instruction Set Manula v2.2](https://content.riscv.org/wp-content/uploads/2017/05/riscv-spec-v2.2.pdf) and a little bit of imagination. :)  
